#+Title: A repository for policies used in demos

* Creating a new branch
- Checkout the repository
- Create a new branch without history
- Add a README where instructions can be placed
- Create =.gitignore= and =.gitattributes= files with necessary content

#+BEGIN_SRC bash :dir /tmp/ :var DEMOBRANCH="My-New-Branch" :results output :wrap EXAMPLE
  DemoBranch=$DEMOBRANCH
  git clone https://gitlab.com/nickanderson/mission-portal-demo.git 
  cd mission-portal-demo
  git checkout --orphan $DemoBranch 
  git rm -rf .
  touch README
  cat <<EOF > .gitignore
  cf_promises_release_id
  cf_promises_validated
  EOF
  cat <<EOF > .gitattributes
  *.png binary
  *.jpg binary
  *.jpeg binary
  *.gif binary
  EOF
  git add README .gitignore .gitattributes
  git commit -m "Initalize new demo branch"
  echo "New Branch '$DemoBranch' initialized. Don't forget to publish your branch (git push)."
#+END_SRC

* Seed the branch with MPF policy

** From source using git

- Clone the MPF
- Checkout the branch, commit or tag which you want to base your policy on
- Install the MPF into a temporary location
- Move masterfiles content from temporary install location into new demo branch
- Configure MPF for DEMO

#+BEGIN_SRC bash :var CHECKOUT="master" :exports both :wrap example :results output
  DEPLOYDIR=$(pwd)
  echo $DEPLOYDIR
  REFLOG=$CHECKOUT
  BUILDROOT=$(mktemp --directory)
  mkdir -p "$BUILDROOT/install"
  echo $BUILDROOT
  cd $BUILDROOT
  git clone https://github.com/cfengine/masterfiles.git
  cd masterfiles
  git checkout $REFLOG
  ./autogen.sh
  make clean
  ./configure --prefix="$BUILDROOT/install"
  EXPLICIT_VERSION="MPF from $REFLOG" make install
  echo "Seeding $DEMOREPO with MPF from $REFLOG"
  echo cp -Rv $BUILDROOT/install/masterfiles/* $DEPLOYDIR
  cp -Rv $BUILDROOT/install/masterfiles/* $DEPLOYDIR
  echo "Seeded MPF from $REFLOG to $DEMOREPO. Don't forget to commit and publish your branch (git push)";
  rm -rf $BUILDROOT
#+END_SRC

#+RESULTS:
#+BEGIN_example
/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo
/tmp/tmp.rainSKk783
Your branch is up-to-date with 'origin/master'.
./autogen.sh: Running determine-version.py ...
./autogen.sh: Running autoreconf ...
/tmp/tmp.rainSKk783/masterfiles
checking build system type... x86_64-pc-linux-gnu
checking host system type... x86_64-pc-linux-gnu
checking target system type... x86_64-pc-linux-gnu
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a thread-safe mkdir -p... /bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... yes
checking whether make supports nested variables... yes
checking whether UID '1000' is supported by ustar format... yes
checking whether GID '1000' is supported by ustar format... yes
checking how to create a ustar tar archive... gnutar
checking if GNU tar supports --hard-dereference... yes
checking whether to enable maintainer-specific portions of Makefiles... yes
checking whether make supports nested variables... (cached) yes
checking for pkg_install... no
checking for shunit2... no

Summary:
Version              -> 3.13.0a.72644da
Core directory       -> not set - tests are disabled
Enterprise directory -> not set - some tests are disabled
Install prefix       -> /var/cfengine

configure: generating makefile targets
checking that generated files are newer than configure... done
configure: creating ./config.status
config.status: creating Makefile
config.status: creating controls/update_def.cf
config.status: creating modules/packages/Makefile
config.status: creating promises.cf
config.status: creating standalone_self_upgrade.cf
config.status: creating tests/acceptance/Makefile
config.status: creating tests/unit/Makefile

DONE: Configuration done. Run "make install" to install CFEngine Masterfiles.

Making clean in modules/packages
make[1]: Entering directory '/tmp/tmp.rainSKk783/masterfiles/modules/packages'
make[1]: Nothing to be done for 'clean'.
make[1]: Leaving directory '/tmp/tmp.rainSKk783/masterfiles/modules/packages'
Making clean in tests/unit
make[1]: Entering directory '/tmp/tmp.rainSKk783/masterfiles/tests/unit'
test -z "test_package_module_apt_get.log test_package_module_yum.log" || rm -f test_package_module_apt_get.log test_package_module_yum.log
test -z "test_package_module_apt_get.trs test_package_module_yum.trs" || rm -f test_package_module_apt_get.trs test_package_module_yum.trs
test -z "test-suite.log" || rm -f test-suite.log
make[1]: Leaving directory '/tmp/tmp.rainSKk783/masterfiles/tests/unit'
make[1]: Entering directory '/tmp/tmp.rainSKk783/masterfiles'
make[1]: Nothing to be done for 'clean-am'.
make[1]: Leaving directory '/tmp/tmp.rainSKk783/masterfiles'
checking build system type... x86_64-pc-linux-gnu
checking host system type... x86_64-pc-linux-gnu
checking target system type... x86_64-pc-linux-gnu
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a thread-safe mkdir -p... /bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... yes
checking whether make supports nested variables... yes
checking whether UID '1000' is supported by ustar format... yes
checking whether GID '1000' is supported by ustar format... yes
checking how to create a ustar tar archive... gnutar
checking if GNU tar supports --hard-dereference... yes
checking whether to enable maintainer-specific portions of Makefiles... yes
checking whether make supports nested variables... (cached) yes
checking for pkg_install... no
checking for shunit2... no

Summary:
Version              -> 3.13.0a.72644da
Core directory       -> not set - tests are disabled
Enterprise directory -> not set - some tests are disabled
Install prefix       -> /tmp/tmp.rainSKk783/install

configure: generating makefile targets
checking that generated files are newer than configure... done
configure: creating ./config.status
config.status: creating Makefile
config.status: creating controls/update_def.cf
config.status: creating modules/packages/Makefile
config.status: creating promises.cf
config.status: creating standalone_self_upgrade.cf
config.status: creating tests/acceptance/Makefile
config.status: creating tests/unit/Makefile

DONE: Configuration done. Run "make install" to install CFEngine Masterfiles.

Making install in modules/packages
make[1]: Entering directory '/tmp/tmp.rainSKk783/masterfiles/modules/packages'
make[2]: Entering directory '/tmp/tmp.rainSKk783/masterfiles/modules/packages'
make[2]: Nothing to be done for 'install-exec-am'.
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/modules/packages'
 /usr/bin/install -c apt_get yum pkg nimclient pkgsrc freebsd_ports zypper '/tmp/tmp.rainSKk783/install/modules/packages'
make[2]: Leaving directory '/tmp/tmp.rainSKk783/masterfiles/modules/packages'
make[1]: Leaving directory '/tmp/tmp.rainSKk783/masterfiles/modules/packages'
Making install in tests/unit
make[1]: Entering directory '/tmp/tmp.rainSKk783/masterfiles/tests/unit'
make[2]: Entering directory '/tmp/tmp.rainSKk783/masterfiles/tests/unit'
make[2]: Nothing to be done for 'install-exec-am'.
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/tmp/tmp.rainSKk783/masterfiles/tests/unit'
make[1]: Leaving directory '/tmp/tmp.rainSKk783/masterfiles/tests/unit'
make[1]: Entering directory '/tmp/tmp.rainSKk783/masterfiles'
make[2]: Entering directory '/tmp/tmp.rainSKk783/masterfiles'
make[2]: Nothing to be done for 'install-exec-am'.
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core'
 /usr/bin/install -c -m 644  ./cfe_internal/core/main.cf ./cfe_internal/core/limit_robot_agents.cf ./cfe_internal/core/host_info_report.cf ./cfe_internal/core/log_rotation.cf '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/templates'
 /usr/bin/install -c -m 644  ./cfe_internal/enterprise/templates/runalerts.sh.mustache ./cfe_internal/enterprise/templates/runalerts.php.mustache ./cfe_internal/enterprise/templates/httpd.conf.mustache '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/templates'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/inventory'
 /usr/bin/install -c -m 644  ./inventory/debian.cf ./inventory/os.cf ./inventory/aix.cf ./inventory/lsb.cf ./inventory/freebsd.cf ./inventory/generic.cf ./inventory/linux.cf ./inventory/any.cf ./inventory/suse.cf ./inventory/redhat.cf ./inventory/windows.cf ./inventory/macos.cf '/tmp/tmp.rainSKk783/install/masterfiles/inventory'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/deprecated'
 /usr/bin/install -c -m 644  ./cfe_internal/core/deprecated/cfengine_processes.cf '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/deprecated'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/services/autorun'
 /usr/bin/install -c -m 644  ./services/autorun/hello.cf '/tmp/tmp.rainSKk783/install/masterfiles/services/autorun'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/lib'
 /usr/bin/install -c -m 644  ./lib/stdlib.cf ./lib/storage.cf ./lib/monitor.cf ./lib/bundles.cf ./lib/cfe_internal_hub.cf ./lib/processes.cf ./lib/files.cf ./lib/users.cf ./lib/cfengine_enterprise_hub_ha.cf ./lib/autorun.cf ./lib/databases.cf ./lib/cfe_internal.cf ./lib/paths.cf ./lib/services.cf ./lib/event.cf ./lib/common.cf ./lib/commands.cf ./lib/feature.cf ./lib/reports.cf ./lib/guest_environments.cf ./lib/packages.cf ./lib/examples.cf ./lib/vcs.cf ./lib/edit_xml.cf ./lib/testing.cf '/tmp/tmp.rainSKk783/install/masterfiles/lib'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update'
 /usr/bin/install -c -m 644  ./cfe_internal/update/lib.cf ./cfe_internal/update/cfe_internal_local_git_remote.cf ./cfe_internal/update/update_bins.cf ./cfe_internal/update/cfe_internal_dc_workflow.cf ./cfe_internal/update/update_policy.cf ./cfe_internal/update/cfe_internal_update_from_repository.cf ./cfe_internal/update/systemd_units.cf ./cfe_internal/update/update_processes.cf '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/controls'
 /usr/bin/install -c -m 644  ./controls/def_inputs.cf ./controls/cf_serverd.cf ./controls/cf_monitord.cf ./controls/cf_runagent.cf ./controls/cf_hub.cf ./controls/update_def_inputs.cf ./controls/cf_execd.cf ./controls/def.cf ./controls/cf_agent.cf ./controls/reports.cf ./controls/update_def.cf '/tmp/tmp.rainSKk783/install/masterfiles/controls'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/ha'
 /usr/bin/install -c -m 644  ./cfe_internal/enterprise/ha/ha_update.cf ./cfe_internal/enterprise/ha/ha_def.cf ./cfe_internal/enterprise/ha/ha.cf '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/ha'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/lib/3.7'
 /usr/bin/install -c -m 644  lib/3.7/README.md '/tmp/tmp.rainSKk783/install/masterfiles/lib/3.7'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal'
 /usr/bin/install -c -m 644  ./cfe_internal/CFE_cfengine.cf '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal'
 /usr/bin/install -c -m 644  ./update.cf ./promises.cf ./standalone_self_upgrade.cf '/tmp/tmp.rainSKk783/install/masterfiles/.'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/sketches/meta'
 /usr/bin/install -c -m 644  ./sketches/meta/api-runfile.cf '/tmp/tmp.rainSKk783/install/masterfiles/sketches/meta'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/watchdog'
 /usr/bin/install -c -m 644  ./cfe_internal/core/watchdog/watchdog.cf '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/watchdog'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/templates'
 /usr/bin/install -c -m 644  ./templates/cfengine3.service.mustache ./templates/cf-serverd.service.mustache ./templates/cf-runalerts.service.mustache ./templates/json_serial.mustache ./templates/cf-hub.service.mustache ./templates/cf-monitord.service.mustache ./templates/cfengine_watchdog.mustache ./templates/cf-apache.service.mustache ./templates/cf-postgres.service.mustache ./templates/cf-consumer.service.mustache ./templates/host_info_report.mustache ./templates/cf-execd.service.mustache ./templates/json_multiline.mustache ./templates/cf-redis-server.service.mustache '/tmp/tmp.rainSKk783/install/masterfiles/templates'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise'
 /usr/bin/install -c -m 644  ./cfe_internal/enterprise/main.cf ./cfe_internal/enterprise/CFE_hub_specific.cf ./cfe_internal/enterprise/file_change.cf ./cfe_internal/enterprise/CFE_knowledge.cf ./cfe_internal/enterprise/mission_portal.cf '/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise'
 /bin/mkdir -p '/tmp/tmp.rainSKk783/install/masterfiles/services'
 /usr/bin/install -c -m 644  ./services/main.cf '/tmp/tmp.rainSKk783/install/masterfiles/services'
make[2]: Leaving directory '/tmp/tmp.rainSKk783/masterfiles'
make[1]: Leaving directory '/tmp/tmp.rainSKk783/masterfiles'
Seeding  with MPF from master
cp -Rv /tmp/tmp.rainSKk783/install/masterfiles/cfe_internal /tmp/tmp.rainSKk783/install/masterfiles/controls /tmp/tmp.rainSKk783/install/masterfiles/inventory /tmp/tmp.rainSKk783/install/masterfiles/lib /tmp/tmp.rainSKk783/install/masterfiles/promises.cf /tmp/tmp.rainSKk783/install/masterfiles/services /tmp/tmp.rainSKk783/install/masterfiles/sketches /tmp/tmp.rainSKk783/install/masterfiles/standalone_self_upgrade.cf /tmp/tmp.rainSKk783/install/masterfiles/templates /tmp/tmp.rainSKk783/install/masterfiles/update.cf /home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/CFE_cfengine.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/CFE_cfengine.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/core'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/main.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/core/main.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/limit_robot_agents.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/core/limit_robot_agents.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/host_info_report.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/core/host_info_report.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/log_rotation.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/core/log_rotation.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/deprecated' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/core/deprecated'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/deprecated/cfengine_processes.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/core/deprecated/cfengine_processes.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/watchdog' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/core/watchdog'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/core/watchdog/watchdog.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/core/watchdog/watchdog.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/main.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/main.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/CFE_hub_specific.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/CFE_hub_specific.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/file_change.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/file_change.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/CFE_knowledge.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/CFE_knowledge.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/mission_portal.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/mission_portal.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/templates' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/templates'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/templates/runalerts.sh.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/templates/runalerts.sh.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/templates/runalerts.php.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/templates/runalerts.php.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/templates/httpd.conf.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/templates/httpd.conf.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/ha' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/ha'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/ha/ha_update.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/ha/ha_update.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/ha/ha_def.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/ha/ha_def.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/enterprise/ha/ha.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/enterprise/ha/ha.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/update'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update/lib.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/update/lib.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update/cfe_internal_local_git_remote.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/update/cfe_internal_local_git_remote.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update/update_bins.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/update/update_bins.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update/cfe_internal_dc_workflow.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/update/cfe_internal_dc_workflow.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update/update_policy.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/update/update_policy.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update/cfe_internal_update_from_repository.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/update/cfe_internal_update_from_repository.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update/systemd_units.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/update/systemd_units.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/cfe_internal/update/update_processes.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/cfe_internal/update/update_processes.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/def_inputs.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/def_inputs.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/cf_serverd.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/cf_serverd.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/cf_monitord.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/cf_monitord.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/cf_runagent.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/cf_runagent.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/cf_hub.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/cf_hub.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/update_def_inputs.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/update_def_inputs.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/cf_execd.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/cf_execd.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/def.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/def.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/cf_agent.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/cf_agent.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/reports.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/reports.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/controls/update_def.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/controls/update_def.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/debian.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/debian.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/os.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/os.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/aix.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/aix.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/lsb.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/lsb.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/freebsd.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/freebsd.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/generic.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/generic.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/linux.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/linux.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/any.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/any.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/suse.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/suse.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/redhat.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/redhat.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/windows.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/windows.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/inventory/macos.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/inventory/macos.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/stdlib.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/stdlib.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/storage.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/storage.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/monitor.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/monitor.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/bundles.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/bundles.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/cfe_internal_hub.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/cfe_internal_hub.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/processes.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/processes.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/files.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/files.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/users.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/users.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/cfengine_enterprise_hub_ha.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/cfengine_enterprise_hub_ha.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/autorun.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/autorun.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/databases.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/databases.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/cfe_internal.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/cfe_internal.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/paths.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/paths.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/services.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/services.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/event.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/event.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/common.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/common.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/commands.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/commands.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/feature.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/feature.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/reports.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/reports.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/guest_environments.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/guest_environments.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/packages.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/packages.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/examples.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/examples.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/vcs.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/vcs.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/edit_xml.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/edit_xml.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/testing.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/testing.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/3.7' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/3.7'
'/tmp/tmp.rainSKk783/install/masterfiles/lib/3.7/README.md' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/lib/3.7/README.md'
'/tmp/tmp.rainSKk783/install/masterfiles/promises.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/promises.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/services' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/services'
'/tmp/tmp.rainSKk783/install/masterfiles/services/main.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/services/main.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/services/autorun' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/services/autorun'
'/tmp/tmp.rainSKk783/install/masterfiles/services/autorun/hello.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/services/autorun/hello.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/sketches' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/sketches'
'/tmp/tmp.rainSKk783/install/masterfiles/sketches/meta' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/sketches/meta'
'/tmp/tmp.rainSKk783/install/masterfiles/sketches/meta/api-runfile.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/sketches/meta/api-runfile.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/standalone_self_upgrade.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/standalone_self_upgrade.cf'
'/tmp/tmp.rainSKk783/install/masterfiles/templates' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cfengine3.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cfengine3.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cf-serverd.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cf-serverd.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cf-runalerts.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cf-runalerts.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/json_serial.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/json_serial.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cf-hub.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cf-hub.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cf-monitord.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cf-monitord.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cfengine_watchdog.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cfengine_watchdog.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cf-apache.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cf-apache.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cf-postgres.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cf-postgres.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cf-consumer.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cf-consumer.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/host_info_report.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/host_info_report.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cf-execd.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cf-execd.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/json_multiline.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/json_multiline.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/templates/cf-redis-server.service.mustache' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/templates/cf-redis-server.service.mustache'
'/tmp/tmp.rainSKk783/install/masterfiles/update.cf' -> '/home/nickanderson/Northern.Tech/CFEngine/cfengine-enterprise-demo/update.cf'
Seeded MPF from master to . Don't forget to commit and publish your branch (git push)
#+END_example

#+BEGIN_SRC sh
  echo "Configuring settings for demo environment"
  cat <<EOF > def.json
  {
    "classes": {
      "enable_client_initiated_reporting": [ "any" ],
      "cfengine_internal_masterfiles_update": [ "any" ],
      "mpf_augments_control_enabled": [ "any" ]
    },
    "vars": {
      "default_data_select_host_monitoring_include": [ ".*" ],
      "default_data_select_policy_hub_monitoring_include": [ ".*" ],
      "control_server_call_collect_interval": "1",
      "control_hub_exclude_hosts": [ "0.0.0.0/0" ],
      "mpf_access_rules_collect_calls_admit_ips": [ "0.0.0.0/0" ],
      "acl": [
        "0.0.0.0/0",
        "::/0"
      ]

    }
  }
  EOF 
  git add def.json
  git commit -m "Configured settings for demo environment"
#+END_SRC

** From release tarball

Download a =Masterfiles ready-to-install tarball= from our Enterprise Releases
or Community Releases page.

#+BEGIN_SRC bash :dir /tmp :var tarball="/tmp/cfengine-masterfiles-3.11.0b1.pkg.tar.gz" :exports both :wrap example :results output
  DEMOREPO=/tmp/mission-portal-demo
  srcTarball=$tarball
  BUILDROOT=$(mktemp --directory)
  mkdir -p "$BUILDROOT/install"
  echo $BUILDROOT
  cd $BUILDROOT
  echo "Seeding $DEMOREPO with MPF from $srcTarball"
  tar --directory "$BUILDROOT/install" --gunzip --extract --file $srcTarball
  echo cp -R $BUILDROOT/install/masterfiles/* $DEMOREPO
  cp -R $BUILDROOT/install/masterfiles/* $DEMOREPO
  cd $DEMOREPO
  git add -A
  git commit -m "MPF from $srcTarball"
  git log
  echo "Seeded MPF from $srcTarball to $DEMOREPO. Don't forget to publish your branch (git push)";
  echo "Configuring settings for demo environment"
  cat <<EOF > def.json
  {
    "classes": {
      "client_initiated_reporting_enabled": [ "any" ],
      "cfengine_internal_masterfiles_update": [ "any" ],
      "mpf_augments_control_enabled": [ "any" ]
    },
    "vars": {
      "default_data_select_host_monitoring_include": [ ".*" ],
      "default_data_select_policy_hub_monitoring_include": [ ".*" ],
      "control_server_call_collect_interval": "1",
      "control_hub_exclude_hosts": [ "0.0.0.0/0" ],
      "mpf_access_rules_collect_calls_admit_ips": [ "0.0.0.0/0" ],
      "acl": [
        "0.0.0.0/0",
        "::/0"
      ]
  
    }
  }
  EOF 
  git add def.json
  git commit -m "Configured settings for demo environment"
#+END_SRC


